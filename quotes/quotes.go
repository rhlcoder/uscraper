package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/gocolly/colly"
)

// Quotes es un struct para organizar todas las capturas de texto de las citas
type Quotes struct {
	QUOTE  string
	AUTHOR string
	TAGS   []string
}

func main() {
	// creo una variable tipo Quotes
	citas := make([]Quotes, 0)

	c := colly.NewCollector()

	// On every div class='quote' call a callback
	c.OnHTML("div.quote", func(e *colly.HTMLElement) {
		cita := Quotes{
			QUOTE:  e.ChildText("span.text"),
			AUTHOR: e.ChildText("small.author"),
			TAGS:   e.ChildTexts("div.tags > a"), // para capturar varios tags en un array uso ChildTexts con 's' al final
		}

		citas = append(citas, cita)

		jsonData, err := json.Marshal(citas)
		if err != nil {
			return
		}

		ioutil.WriteFile("Quotes.json", jsonData, 0644)

	})

	// para ir a la siguiente pagina haciendo click en el boton "next" sugoiii
	c.OnHTML(".next a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if link == "" {
			fmt.Println("Link not found")
		}
		fmt.Printf("link found: %s", link)
		c.Visit(e.Request.AbsoluteURL(link))
	})

	c.OnResponse(func(r *colly.Response) {
		fmt.Println("Visitado", r.Request.URL)
	})

	// Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("holaa", r.URL.String())
	})

	// Start visiting on https://sitio.web
	c.Visit("https://quotes.toscrape.com")
}
