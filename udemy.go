package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

func onlyNumber(t string) int {
	repl := regexp.MustCompile(`\D`)
	i, err := strconv.Atoi(repl.ReplaceAllString(t, ""))
	if err == nil {
		return i
	}
	return 0
}

// Info about the course
type Info struct {
	URL         string
	TITLE       string
	DESCRIPTION string
	RATING      float64
	VOTES       int
	STUDENTS    int
	INSTRUCTORS []string
	UPDATED     string
	TAGS        []string
	LENGTH      string
}

func main() {
	infoList := make([]Info, 0)
	var info Info

	// TODO: parsear desde un archivo externo. Trabajado en ello en mycourses.go
	urls := []string{
		// Algunos ejemplos
		"https://www.udemy.com/course/webpack-5-fundamentals/",
		"https://www.udemy.com/course/communication-skills-class-course-training/",
		"https://www.udemy.com/course/complete-woocommerce-course-wordpress-for-e-commerce/",
		"https://www.udemy.com/course/wordpress-tu-pagina-web-sin-codigo/",
		"https://www.udemy.com/course/wordpress-starter/",
		"https://www.udemy.com/course/the-complete-wordpress-for-beginners-course/",
		"https://www.udemy.com/course/draft/1299532/",
		"https://www.udemy.com/course/online-business-selling-simple-products-amazon/",
	}

	co := colly.NewCollector(
		colly.Async(true),
	)

	// Si no agrego otro collector, el scraper funciona de manera random (?)
	c := co.Clone()

	// -------------
	c.OnResponse(func(r *colly.Response) {
		fmt.Println("Visitado", r.Request.URL)
	})

	c.OnHTML("h1", func(e *colly.HTMLElement) {
		info.TITLE = strings.TrimSpace(e.Text)
	})

	//class="udlite-text-md clp-lead__headline" dos clases en un div
	c.OnHTML("div.clp-lead__headline.udlite-text-md", func(e *colly.HTMLElement) {
		info.DESCRIPTION = strings.TrimSpace(e.Text)
	})

	c.OnHTML("span[data-purpose='rating-number']", func(e *colly.HTMLElement) {
		info.RATING, _ = strconv.ParseFloat(e.Text, 64)
	})

	c.OnHTML("div[data-purpose='rating']", func(e *colly.HTMLElement) {
		re := regexp.MustCompile(`\((\d+,?\d*)`)
		info.VOTES = onlyNumber(re.FindString(e.Text))
	})

	c.OnHTML("div[data-purpose='enrollment']", func(e *colly.HTMLElement) {
		info.STUDENTS = onlyNumber(e.Text)
	})

	c.OnHTML("span.instructor-links--names--7UPZj", func(e *colly.HTMLElement) {
		if len(e.ChildTexts("span")[1:]) >= 1 {
			info.INSTRUCTORS = e.ChildTexts("span")[1:]
		} else {
			info.INSTRUCTORS = []string{e.ChildText("a")}
		}
	})

	c.OnHTML("div.last-update-date > span:nth-child(2)", func(e *colly.HTMLElement) {
		r := regexp.MustCompile(`(?P<Month>\d{1,2})/(?P<Year>\d{4})`)
		date := r.FindStringSubmatch(e.Text)
		if len(date[1]) == 1 {
			date[1] = "0" + date[1]
		}
		info.UPDATED = date[2] + "-" + date[1] + "-01"
	})

	c.OnHTML("div.topic-menu.udlite-breadcrumb", func(e *colly.HTMLElement) {
		info.TAGS = e.ChildTexts("a")
	})

	// TODO: Agregar otro collector para los que tienen formato de hora diferente
	c.OnHTML("span[data-purpose='video-content-length']", func(e *colly.HTMLElement) {
		re := regexp.MustCompile(`(\d+\.?\d+? h)`)
		info.LENGTH = re.FindString(e.Text)
	})

	for _, u := range urls {
		// Este no lo reseteo porque es unico para cada fila
		info.URL = u

		// Start visiting url list
		c.Visit(u)
		fmt.Println(u)
		c.Wait()

		// Add to list for marshaling
		infoList = append(infoList, info)

		// Reset struct before next iteration
		info.TITLE = ""
		info.DESCRIPTION = ""
		info.RATING = -1
		info.VOTES = 0
		info.STUDENTS = 0
		info.INSTRUCTORS = nil
		info.UPDATED = ""
		info.TAGS = nil
		info.LENGTH = ""
	}

	fmt.Println("Writing udemyInfo.json...")

	jsonData, err := json.Marshal(infoList)
	if err != nil {
		return
	}
	ioutil.WriteFile("F:/udemyInfo.json", jsonData, 0644)

	fmt.Println("Finished!")
}
